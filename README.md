# Supportive repository for keystroke dynamics analysis

Completed by Andrey Konstantinov for Scalable Data Science course for University of Canterbury, Christchurch
2016/05

### What is keystroke biometric?
https://en.wikipedia.org/wiki/Keystroke_dynamics

### When keystroke biometric can be usefull?
- detect if an account is shared by multiple people (to enforce licensing)
- detect if an account is accidently used by a second person when a device is left unattended (to block it / reask for password)
- waive the second form authentication factor for less-risky opertions (when keystroke dynamic is strong)
- categorise PC user to experienced and not experienced

### Existing work
Comparing Anomaly-Detection Algorithms for Keystroke Dynamics
- http://www.cs.cmu.edu/~maxion/pubs/KillourhyMaxion09.pdf
- http://www.cs.cmu.edu/~keystroke/

## Scope of this repository

### Dataset 
- data subfolder contains csv file for analysis
- details could be found here http://www.cs.cmu.edu/~keystroke/

### Model definition and verification 
- install `scala` and `sbt` to build and run the project
- run `sbt compile` to build the project

### Demo application 
- check demo subfolder for more details
