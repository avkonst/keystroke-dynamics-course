"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var AppComponent = (function () {
    function AppComponent() {
        this.header = 'Type your password you would like to train';
        this.status = '';
        this.typings = '';
        this.model = undefined;
        this.trainRemaining = 10;
        this.timings = [];
    }
    AppComponent.prototype.onSubmit = function (form) {
        if (this.model === undefined) {
            if (this.typings.length < 8) {
                this.status = 'Think of longer password (at least 8 symbols)';
            }
            else {
                this.status = '';
                this.model = new Model(this.typings);
                this.header = 'Type the same password again to complete training (' + this.trainRemaining + ' remaining)';
            }
        }
        else {
            if (this.typings !== this.model.trainedPassword) {
                this.status = 'Wrong password. Use "' + this.model.trainedPassword + '"';
            }
            else if (this.timings.length > this.typings.length + 1 || this.timings.length < this.typings.length) {
                this.status = 'Retype password again without corrections';
            }
            else {
                this.status = '';
                this.trainRemaining--;
                if (this.trainRemaining > 0) {
                    this.header = 'Type the same password again to complete training (' + this.trainRemaining + ' remaining)';
                    this.model.train(this.timings);
                }
                else if (this.trainRemaining === 0) {
                    this.header = 'Try to login using the same password';
                    this.model.train(this.timings);
                }
                else {
                    this.header = 'Now, ask somebody else to login using the same password';
                    if (this.model.verify(this.timings)) {
                        this.status = 'Keystroke accepted, current creditability: ' + this.model.creditability;
                    }
                    else {
                        this.status = 'Keystroke rejected, current creditability: ' + this.model.creditability;
                    }
                }
            }
        }
        this.typings = '';
        this.timings = [];
    };
    AppComponent.prototype.eventHandler = function (keyCode) {
        if (this.model) {
            this.timings.push(keyCode.timeStamp);
        }
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            template: "\n        <h3>Keystroke Biometric Demo</h3>\n        <h4>{{header}}</h4>\n        <div>\n            <form (ngSubmit)=\"onSubmit(theForm)\" #theForm=\"ngForm\">\n                <input [(ngModel)]=\"typings\" (keypress)=\"eventHandler($event)\" placeholder=\"\">\n                <button type=\"submit\">Login</button>\n                <label>{{status}}</label>\n            </form>        \n        </div>\n    "
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
var Model = (function () {
    function Model(trainedPassword) {
        this.trainedPassword = '';
        this.maxTimingsToKeep = 100;
        this.timings = [];
        this.creditability = 0;
        this.threshold = 0;
        this.trainedPassword = trainedPassword;
    }
    Model.prototype.train = function (timings) {
        this.timings.push(this.extract(timings));
    };
    Model.prototype.verify = function (timings) {
        var newRow = this.extract(timings);
        var representativeTimings = this.getRepresentativeSet(0.2);
        var averageTimings = this.getAverageTimings(representativeTimings);
        this.threshold = this.calculateThreshold(representativeTimings);
        console.log("threshold");
        console.log(this.threshold);
        var threshold = this.threshold * (1.5 - 0.45 * this.timings.length / this.maxTimingsToKeep);
        console.log("adjusted threshold");
        console.log(threshold);
        var distance = this.getDistance(newRow, averageTimings);
        console.log("distance");
        console.log(distance);
        if (distance > threshold) {
            this.creditability = this.creditability - (distance / threshold - 1);
            return false;
        }
        else {
            this.creditability = this.creditability + (1 - distance / threshold);
            // train continously
            if (this.timings.length > this.maxTimingsToKeep) {
                this.timings.shift(); // discard older timings
            }
            this.train(timings); // add more recent
            return true;
        }
    };
    Model.prototype.extract = function (timings) {
        var startTime = timings[0];
        var newRow = [0];
        for (var i = 1; i < this.trainedPassword.length; ++i) {
            newRow.push(timings[i] - timings[i - 1]);
        }
        return newRow;
    };
    Model.prototype.getRepresentativeSet = function (ratio) {
        if (ratio === void 0) { ratio = 0.2; }
        var rowsToExclude = this.timings.length * ratio;
        var timings = this.timings;
        for (var i = 0; i < rowsToExclude; ++i) {
            var averageTimings = this.getAverageTimings(timings);
            var maxDistance = 0;
            var maxDistanceRow = -1;
            var newTimings = [];
            var newTimingsRow = 0;
            for (var row = 0; row < timings.length; ++row) {
                var currentRowDistance = this.getDistance(timings[row], averageTimings);
                if (currentRowDistance > maxDistance) {
                    if (maxDistanceRow >= 0) {
                        newTimings[newTimingsRow] = timings[maxDistanceRow];
                        newTimingsRow++;
                    }
                    maxDistance = currentRowDistance;
                    maxDistanceRow = row;
                }
                else {
                    newTimings[newTimingsRow] = timings[row];
                    newTimingsRow++;
                }
            }
            timings = newTimings;
        }
        return timings;
    };
    Model.prototype.calculateThreshold = function (timings) {
        var _this = this;
        var averageTimings = this.getAverageTimings(timings);
        var distances = timings.map(function (i) { return _this.getDistance(i, averageTimings); }).sort(function (a, b) { return a - b; });
        console.log("distances");
        console.log(distances);
        return distances[distances.length - 1];
    };
    Model.prototype.getAverageTimings = function (timings) {
        var result = [];
        for (var col = 0; col < this.trainedPassword.length; ++col) {
            result.push(0);
        }
        for (var col = 0; col < this.trainedPassword.length; ++col) {
            for (var row = 0; row < timings.length; ++row) {
                result[col] = result[col] + timings[row][col];
            }
        }
        for (var col = 0; col < this.trainedPassword.length; ++col) {
            result[col] = result[col] / timings.length;
        }
        return result;
    };
    Model.prototype.getDistance = function (array1, array2) {
        var result = 0;
        for (var i = 0; i < array1.length; ++i) {
            result = result + Math.abs(array1[i] - array2[i]);
        }
        return result;
    };
    return Model;
}());
//# sourceMappingURL=app.component.js.map