import {Component} from '@angular/core';
import { NgForm }  from '@angular/common';

@Component({
    selector: 'my-app',
    template: `
        <h3>Keystroke Biometric Demo</h3>
        <h4>{{header}}</h4>
        <div>
            <form (ngSubmit)="onSubmit(theForm)" #theForm="ngForm">
                <input [(ngModel)]="typings" (keypress)="eventHandler($event)" placeholder="">
                <button type="submit">Login</button>
                <label>{{status}}</label>
            </form>        
        </div>
    `
})
export class AppComponent {
    header = 'Type your password you would like to train';
    status = '';
    typings = '';
    model: Model = undefined;
    trainRemaining = 10;
    timings: Array<number> = [];

    onSubmit(form: NgForm) {
        if (this.model === undefined) {
            if (this.typings.length < 8) {
                this.status = 'Think of longer password (at least 8 symbols)';
            } else {
                this.status = '';
                this.model = new Model(this.typings);
                this.header = 'Type the same password again to complete training (' + this.trainRemaining + ' remaining)';
            }
        } else {
            if (this.typings !== this.model.trainedPassword) {
                this.status = 'Wrong password. Use "' + this.model.trainedPassword + '"';
            } else if (this.timings.length > this.typings.length + 1 || this.timings.length < this.typings.length) {
                this.status = 'Retype password again without corrections';
            } else {
                this.status = '';

                this.trainRemaining--;
                if (this.trainRemaining > 0) {
                    this.header = 'Type the same password again to complete training (' + this.trainRemaining + ' remaining)';

                    this.model.train(this.timings);
                } else if (this.trainRemaining === 0) {
                    this.header = 'Try to login using the same password';

                    this.model.train(this.timings);
                } else {
                    this.header = 'Now, ask somebody else to login using the same password';

                    if (this.model.verify(this.timings)) {
                        this.status = 'Keystroke accepted, current creditability: ' + this.model.creditability;
                    } else {
                        this.status = 'Keystroke rejected, current creditability: ' + this.model.creditability;
                    }
                }
            }
        }
        this.typings = '';
        this.timings = [];
    }

    eventHandler(keyCode: KeyboardEvent) {
        if (this.model) {
            this.timings.push(keyCode.timeStamp);
        }
    }
}

class Model {
    public trainedPassword: string = '';
    private maxTimingsToKeep = 100;
    private timings: number[][] = [];
    public creditability = 0;
    private threshold = 0;

    constructor(trainedPassword: string) {
        this.trainedPassword = trainedPassword;
    }

    train(timings: Array<number>): void {
        this.timings.push(this.extract(timings));
    }

    verify(timings: Array<number>): boolean {
        const newRow = this.extract(timings);
        const representativeTimings = this.getRepresentativeSet(0.2);
        const averageTimings = this.getAverageTimings(representativeTimings);
        this.threshold = this.calculateThreshold(representativeTimings);
        console.log("threshold");
        console.log(this.threshold);
        const threshold = this.threshold * (1.5 - 0.45 * this.timings.length / this.maxTimingsToKeep);
        console.log("adjusted threshold");
        console.log(threshold);
        const distance = this.getDistance(newRow, averageTimings);
        console.log("distance");
        console.log(distance);
        if (distance > threshold) {
            this.creditability = this.creditability - (distance / threshold - 1);
            return false;
        } else {
            this.creditability = this.creditability + (1 - distance / threshold);
            // train continously
            if (this.timings.length > this.maxTimingsToKeep) {
                this.timings.shift(); // discard older timings
            }
            this.train(timings);  // add more recent
            return true;
        }
    }

    private extract(timings: number[]): number[] {
        const startTime = timings[0];
        const newRow: number[] = [0];
        for (let i = 1; i < this.trainedPassword.length; ++i) {
            newRow.push(timings[i] - timings[i - 1]);
        }
        return newRow;
    }

    private getRepresentativeSet(ratio = 0.2) {
        const rowsToExclude = this.timings.length * ratio;
        let timings = this.timings;
        for (let i = 0; i < rowsToExclude; ++i) {
            const averageTimings = this.getAverageTimings(timings);
            let maxDistance = 0;
            let maxDistanceRow = -1;
            const newTimings: number[][] = [];
            let newTimingsRow = 0;
            for (let row = 0; row < timings.length; ++row) {
                const currentRowDistance = this.getDistance(timings[row], averageTimings);
                if (currentRowDistance > maxDistance) {
                    if (maxDistanceRow >= 0) {
                        newTimings[newTimingsRow] = timings[maxDistanceRow];
                        newTimingsRow++;
                    }
                    maxDistance = currentRowDistance;
                    maxDistanceRow = row;
                } else {
                    newTimings[newTimingsRow] = timings[row];
                    newTimingsRow++;
                }
            }
            timings = newTimings;
        }
        return timings;
    }

    private calculateThreshold(timings: number[][]): number {
        const averageTimings = this.getAverageTimings(timings);
        let distances = timings.map(i => this.getDistance(i, averageTimings)).sort((a, b) => a - b);
        console.log("distances");
        console.log(distances);
        return distances[distances.length - 1];
    }

    private getAverageTimings(timings: number[][]): number[] {
        const result: number[] = [];
        for (let col = 0; col < this.trainedPassword.length; ++col) {
            result.push(0);
        }
        for (let col = 0; col < this.trainedPassword.length; ++col) {
            for (let row = 0; row < timings.length; ++row) {
                result[col] = result[col] + timings[row][col];
            }
        }
        for (let col = 0; col < this.trainedPassword.length; ++col) {
            result[col] = result[col] / timings.length;
        }
        return result;
    }

    private getDistance(array1: number[], array2: number[]): number {
        let result = 0;
        for (let i = 0; i < array1.length; ++i) {
            result = result + Math.abs(array1[i] - array2[i]);
        }
        return result;
    }
}
